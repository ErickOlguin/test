
def leer():
    """
    Declaramos una variable global, en este caso letra,
    posteriormente solicitamos un texto y aplicamos 
    formato en minuscula.
    """
    global letra
    letra =(input('Dame una palabra: ')).lower()
def contador():
    """
    Funcion principal, que va a contar todas las vocales de una frase 
    o palabra
    """
    vocales = ['a','e','i','o','u'] #Lista con vocales
    cont = 0 #Contador
    for i in vocales: #Recorremos la lista llamada vocales
        for j in letra: #Recorremos lo que contenga la frase o palabra introducida 
            if(i==j): #Comparacion de caracteres con lo que tenemos en nuestra lista vocales
                cont+=1 #El contador va a ir incrementando con respecto a las vocales que encuentre en dicha frase o palabra 
    print('El numero de vocales es: ',cont)

if __name__ == '__main__': #Mando llamar is dos funciones
    leer()
    contador()

