from pymongo import MongoClient

conexion = MongoClient('localhost',27017) #Conexion base de datos local
Base = conexion.Pruebas #Conexion a la coleccion llamada Pruebas
dic = {'Inventario':{
						'Número':int(),
						'Fecha':ISODate(),
						'Usuario':str(),
						'Producto':{
									'Descripcion':str(),
									'Cantidad':int(),
									'Precio':float()
									},
						'Almacen':int()
					}
		}
'''
Diccionario con valores normalizados 
'''
Base.insert_one(dic) #Inserta documento en la coleccion llamada Prueba