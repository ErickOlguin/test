from django.db import models
from django.utils import timezone
from django.conf import settings

# Create your models here.
class Post(models.Model): # Nombre de la tabla en nuestra base de datos, estamos heredando de una clase Padre y nuestro Post es una clase hijo. Así ya no tendremos que reinventar o reescribir código.
	author = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete =models.CASCADE)
	title = models.CharField(max_length=200)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	published_at = models.DateTimeField(blank=True, null=True)
	is_published = models.BooleanField(default=False)

	def publish(self):
		self.published_at = timezone.now()
		self.is_published = True
		self.save()


	def __str__(self):
		return self.title
