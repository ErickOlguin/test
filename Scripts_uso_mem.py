import psutil
import os
import time

"""uso_mem.py
Te imprime en pantalla cuanta Memoria RAM y de CPU estas 
ocupando a la hora de correr varios procesos.
"""
while True:
	time.sleep(3)
	report={}
	ram=psutil.virtual_memory()
	disk=psutil.disk_io_counters()
	net=psutil.net_io_counters()
	report["CPU_PERCENT"]=psutil.cpu_percent()
	report["RAM"]=(ram.total-ram.available)/1000000000
	#report["READ_BYTES"]=disk.read_bytes
	#report["WRITE_BYTES"]=disk.write_bytes
	#report["NET_OUT"]=net.bytes_sent
	#report["NET_IN"]=net.bytes_recv
	#report["PACK_OUT"]=net.packets_sent
	#report["PACK_IN"]=net.packets_recv
	print(report)
