# Test Python Developer.

En este Proyecto se encontraran los ejercicios adjuntos en el archivo pdf enviado al correo erick_eduardo_olguin@hotmail.com


## Pasos para creacion de Base de datos con Django.
- Django
    -[Windows](https://migantoju.com/2018/12/23/crea-tu-primera-aplicacion-con-django-framework/)


## Codigos.
- ejercicio.py(Contar cuantas vocales hay)
- ejercicio2.py(Reemplazar las vocales por la vocal consecutiva (a->e).)
- ejercicio3.py(No se realizo ya que no se anexo la figura A)
- models.py(Realiza el esquema DB (Relacional o NoSql) para lo siguiente:
• Aplicación para gestionar una flota de vehículos, en donde una persona puede
tener n vehículos asignados pero solo puede manejar uno. Cada asignación de
vehículo tiene una fecha de expiración)
- normalizacion.py(Dado las siguientes entidades y atributos, normaliza como creas conveniente.
Inventario
• Número.
• Fecha.
• Usuario.
• Producto:
o Descripción.
o Cantidad
o Precio
• Almacén. 
)
- jueguito.py(Intento de juego de badminton)
- ejercicio7(No se realizo ya que no  obtuve respuesta de que es un HU)
- Scripts_uso_mem.py(Anexo codigo para medir cuanto se ocupa de ram al ejecutar los codigos)


## Dudas y/o comentarios.
- Quisiera saber si me podrian comentar en el ultimo ejercicio no entendi a que se referia con un HU, estuve buscando pero no encontre nada relacionado(Duda).
- No se me hizo llegar la Figura A(Comentario).
- Les envio esto antes de las 12, puesto que se cumple el lapso maximo de entrega de 2 dias(Comentario).

